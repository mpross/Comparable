package com.poofycow.utils.comparator;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

public class Comparator {
    public static boolean compare(Comparable a, Comparable b) throws IllegalAccessException {
        if (!a.getClass().equals(b.getClass())) {
            return false;
        }
        
        return determine(a, b);
    }
    
    private static boolean determine(Object a, Object b) throws IllegalAccessException {
        if (a.getClass().isArray()) {
            return compareArray(a, b);
        } else if (a instanceof Collection) {
            return compareCollection((Collection<?>) a, (Collection<?>) b);
        } else if (a instanceof Map) {
            return compareMap((Map<?, ?>) a, (Map<?, ?>) b);
        } else {
            return compareSimple(a, b);
        }
    }
    
    private static boolean compareArray(Object a, Object b) {
        Object[] col1 = (Object[]) a;
        Object[] col2 = (Object[]) b;
        
        for (int i = 0; i < col1.length; i++) {
            if (col1[i] instanceof Comparable) {
                if (!((Comparable) col1[i]).compare((Comparable) col2[i]))
                    return false;
            } else {
                if (!col1[i].equals(col2[i]))
                    return false;
            }
        }
        
        return true;
    }
    
    private static boolean compareCollection(Collection<?> a, Collection<?> b) {
        Object[] col1 = ((Collection<?>) a).toArray();
        Object[] col2 = ((Collection<?>) b).toArray();
        
        if (col1.length != col2.length)
            return false;
        
        for (int i = 0; i < col1.length; i++) {
            if (col1[i] instanceof Comparable) {
                if (!((Comparable) col1[i]).compare((Comparable) col2[i]))
                    return false;
            } else {
                if (!col1[i].equals(col2[i]))
                    return false;
            }
        }
        
        return true;
    }
    
    private static boolean compareMap(Map<?, ?> a, Map<?, ?> b) throws IllegalAccessException {
        Object[] keys1 = a.keySet().toArray();
        Object[] keys2 = b.keySet().toArray();
        Object[] vals1 = a.values().toArray();
        Object[] vals2 = b.values().toArray();
        
        for (int i = 0; i < keys1.length; i++) {
            if (keys1[i] instanceof Comparable) {
                if (!((Comparable) keys1[i]).compare((Comparable) keys2[i]))
                    return false;
            } else {
                if (!keys1[i].equals(keys2[i]))
                    return false;
            }
            
            if (vals1[i] instanceof Comparable) {
                if (!((Comparable) vals1[i]).compare((Comparable) vals2[i]))
                    return false;
            } else {
                if (!determine(vals1[i], vals2[i]))
                    return false;
            }
        }
        
        return true;
    }
    
    private static boolean compareSimple(Object a, Object b) throws IllegalAccessException {
        Field[] fields = a.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            
            try {
                Object val1 = field.get(a);
                Object val2 = field.get(b);
                
                if (val1 instanceof Comparable) {
                    if (!((Comparable) val1).compare((Comparable) val2))
                        return false;
                } else {
                    if (val1 instanceof Collection<?> || val1 instanceof Map<?, ?> || val1.getClass().isArray()) {
                        if (!determine(val1, val2))
                            return false;
                    } else if (!val1.equals(val2)) {
                        return false;
                    }
                }
            } catch (IllegalAccessException e) {
                throw e;
            }
        }
        
        return true;
    }
    
}
